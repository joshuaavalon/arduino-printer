import win32ui
from pathlib import Path
from random import randrange
from typing import Callable, List

from serial import Serial


class Sensor:
    def __init__(self, port: str = "COM3", baudrate: int = 9600, threshold: int = 500):
        self._port = port  # type: str
        self._baudrate = baudrate  # type: int
        self._threshold = threshold  # type: int

    def run(self, callback: Callable[[int], bool]):
        serial = Serial(port=self._port, baudrate=self._baudrate)
        should_loop = True
        while should_loop:
            response = serial.readline()  # type: str
            try:
                value = int(response)
                should_loop = callback(value < self._threshold)
            except ValueError:
                pass


class Printer:
    def __init__(self, name: str, left_margin: int, top_margin: int):
        self._name = name  # type: str
        self._left_margin = left_margin  # type: int
        self._top_margin = top_margin  # type: int

    def print(self, content: str):
        doc = win32ui.CreateDC()
        doc.CreatePrinterDC(self._name)
        doc.StartDoc("")
        doc.StartPage()
        top_margin = self._top_margin
        for line in content.split("\n"):
            doc.TextOut(self._left_margin, top_margin, line)
            top_margin += 50
        doc.EndPage()
        doc.EndDoc()


class ContentFactory:
    def __init__(self, path: Path):
        if not path.is_dir():
            raise ValueError(f"{path} is not a directory")
        self._data_list = []  # type: List[str]
        self._used_data_list = []  # type: List[str]
        text_files = (p for p in path.iterdir() if p.suffix == ".txt")
        for text_file in text_files:
            with open(text_file) as file:
                self._data_list.append(file.read())

    def get_random_content(self) -> str:
        if len(self._data_list) <= 0:
            self._data_list = self._used_data_list
            self._used_data_list = []
        index = randrange(0, len(self._data_list))
        item = self._data_list.pop(index)
        self._used_data_list.append(item)
        return item


class App:
    def __init__(self, sensor: Sensor, printer: Printer, factory: ContentFactory, min_num_of_detect: int,
                 max_num_of_detect: int):
        self._sensor = sensor  # type: Sensor
        self._printer = printer  # type: Printer
        self._factory = factory  # type: ContentFactory
        self._detect = 0  # type: int
        self._last_detected = False  # type: bool
        self._min_num_of_detect = min_num_of_detect  # type: int
        self._max_num_of_detect = max_num_of_detect  # type: int
        self._num_of_detect = self._get_num_of_detect()  # type: int

    def run(self):
        self._detect = 0
        self._last_detected = False
        self._sensor.run(self._on_detect)

    def _on_detect(self, detected: bool) -> bool:
        changed = self._last_detected != detected
        self._last_detected = detected
        if not changed:
            return True
        if detected:
            self._detect += 1
        if self._detect >= self._num_of_detect:
            content = self._factory.get_random_content()
            self._printer.print(content)
            self._detect = 0
            self._num_of_detect = self._get_num_of_detect()
        print(f"Detect: {self._detect}")
        return True

    def _get_num_of_detect(self) -> int:
        num = randrange(self._min_num_of_detect, self._max_num_of_detect)
        print(f"Need Detect: {num}")
        return num


def main():
    sensor = Sensor(port="COM3", baudrate=9600)
    printer = Printer("Xprinter XP-365B", 50, 50)
    factory = ContentFactory(Path("content"))
    app = App(sensor, printer, factory, 5, 20)
    app.run()


if __name__ == "__main__":
    main()
