# Arduino Printer

This is a demo project using Arduino, Python and Xprinter XP-365B.

## Requirements

* [Python 3.6](https://www.python.org/downloads/)
* [Arduino 1.8.5](https://www.arduino.cc/en/Main/Software)
* [Xprinter XP-365B](https://www.seagullscientific.com/drivers/printer-driver-features/?m=xprinter+xp-365b)

> This project runs on Windows only

## Configuration

1. Use Arduino 1.8.5 to deploy `arduino.ino` to Arduino with IR Sensor.
2. Install driver for Xprinter XP-365B
3. Go to device manager and confirm the USB port and change the port accordingly in `app.py` 

## Usage

1. Put your own `.txt` inside a `content` folder.
2. Run 

```sh
python app.py
```